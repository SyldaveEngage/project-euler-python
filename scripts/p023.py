#sum of every number that can't be written as the sum of two abundant numbers

def divisors(number):
    divisors_array , tmpNumber = [1] , number
    for i in range(2 , int(number**0.5)+1):
        if not number%i:
            divisors_array += [i] if i == number/i else [i, number//i]
    return divisors_array

def is_abundant(number):
    return sum(divisors(number)) > number

def is_sum_of_abundants(number):
    for i in range(1 , number/2+1):
        if is_abundant(i) and is_abundant(number-i):
            return True
    return False

total_sum = 0

for i in range(1, 28123):
    if not is_sum_of_abundants(i):
        total_sum += i

print(total_sum)