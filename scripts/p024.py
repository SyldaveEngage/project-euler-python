#position'th permutation of an array numbers

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
order = []
position = 1000000

def factorial(number):
    if number <= 1:
        return 1
    else:
        return number*factorial(number-1)

def index_of_next_number(numbers, order, position):
    tmp_num = factorial(9-len(order))
    for i in range(0, len(numbers)):
        if (i+1)*tmp_num >= position:
            order += [numbers.pop(i)]
            position -= (i)*tmp_num
            break
    return numbers, order, position


while len(numbers):
    numbers, order, position = index_of_next_number(numbers, order, position)

print(order)